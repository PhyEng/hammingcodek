﻿//Decoder
#include <iostream>
#include <string>
#include <vector>
#include <bitset>
#include <fstream> //работа с файлами
#include <iomanip> // манипуляторы ввода/вывода

using namespace std;

//функция возведения в степень
int my_pow(int x, int y)
{
	if (y == 0) return 1;
	int product = x;
	for (int i = 1; i < y; i++) {
		product *= x;
	}
	return product;
}

//функция вычисления контрольных битов и исправления ошибок
void calculate(vector<char>& block) {
	vector<int> calc_bit = { 0, 0, 0, 0 };
	vector<int> mistake_bit = { 0, 0, 0, 0 };
	//вычисление ошибочных позиций битов
	for (int i = 0; i < 4; i++) {
		mistake_bit[i] = (int)(block[my_pow(2, i) - 1]) - 48;
	}
	//зануление массива с битами
	for (int i = 0; i < 4; i++) {
		block[(__int64)my_pow(2, i) - 1] = '0';
	}
	//рассчет проверочных битов
	for (int i = 0; i < 4; i++) {
		for (int j = my_pow(2, i) - 1; j < 12; j += my_pow(2, i + 1)) {
			for (int k = j; k < (j + my_pow(2, i)); k++) {
				if ((k < 12) and (i < 12)) {
					calc_bit[i] += (int)block[k];
				}
				else break;
			}
		}
		calc_bit[i] = calc_bit[i] % 2;
	}
	//поиск ошибки в битах
	int mistakes = 0;
	for (int i = 0; i < 3; i++) {
		if (calc_bit[i] != mistake_bit[i]) {
			mistakes += my_pow(2, i);
		}
	}
	//исправление ошибки
	if (mistakes != 0) {
		block[(__int64)mistakes - 1] = (char)(((int)block[(__int64)mistakes - 1] + 1) % 2) + '0';
	}
	//удаление контрольных бит
	for (int i = 3; i >= 0; i--) {
		block.erase(block.begin() + my_pow(2, i) - 1);
		vector<char>(block).swap(block);
	}
}

int main() {

    string buff;
	string coded_string;

	ifstream fin("signal.txt");			//открываем файл для чтения зашифрованных данных
	
	while (getline(fin, buff))	//построчное чтение строк
	{
		coded_string += buff + '\n';
	}
	fin.close();

	vector<char> coded_bits;
	vector<char> block_of_bits;

	for (int i = 0; i < coded_string.size(); i++) {
		block_of_bits.push_back(coded_string[i]);
		if (block_of_bits.size() % 12 == 0) {
			calculate(block_of_bits);				//вычисляем контрольные биты
			coded_bits.insert(						//добавляем в итоговый массив
				coded_bits.end(),
				block_of_bits.begin(),
				block_of_bits.end()
			);
			block_of_bits = {};
		}
	}

	//вывод данных
	string string_bits;  // биты в строки
	string word;

	for (int i = 0; i < coded_bits.size(); ++i) {
		string_bits += coded_bits[i];
		if (string_bits.size() % 8 == 0) {
			// функция 'to_ulong' вернет из битов изначальный символ
			word += char(bitset<8>(string_bits).to_ulong());
			string_bits.clear();
		}
	}

	ofstream fout("signal.txt", ios_base::out);	//открываем файл для записи данных
	fout << "Encoded data: " << endl;
	for (auto now : coded_bits) fout << now;
	fout << endl;
	fout << "Decoded data:" << endl;
	fout << word;						//запись декодированных данных в файл
	fout.close();

	cout << "Operation completed successfully";
	return 0;
}

﻿//Encoder

#include <iostream>
#include <string>
#include <vector>
#include <bitset>
#include <fstream> //работа с файлами
#include <iomanip> // манипуляторы ввода/вывода

using namespace std;

//функция возведения в степень
int my_pow(int x, int y)
{
	if (y == 0) return 1;
	int product = x;
	for (int i = 1; i < y; i++) {
		product *= x;
	}
	return product;
}

//функция вычисления контрольных битов
void calculate(vector<char> &block) {
	vector<int> calc_bit = {0, 0, 0, 0};
	for (int i = 0; i < 4; i++) {
		for (int j = my_pow(2, i) - 1; j < 12; j += my_pow(2, i + 1)) {
			for (int k = j; k < (j + my_pow(2, i)); k++) {
				if ((k < 12) and (i < 12)) {
					calc_bit[i] += (int)block[k];   //Подсчёт проверочных бит
				}
				else break;
			}
		}
		calc_bit[i] = calc_bit[i] % 2;
	}
	for (int i = 0; i < 4; i++) {
		block[my_pow(2, i) - 1] = (char)calc_bit[i] + '0';  //Добавляем проверочные биты
	}
}

int main() {

	string buff;
	string word;
	
	ifstream fin("signal.txt");		//открываем файл для чтения данных
	while (getline(fin, buff))		//построчное чтение строк
	{
		word += buff + '\n';
	}
	fin.close();

	string string_bits;

	for (int i = 0; i < word.size(); ++i)
	{	// после конвертации в биты, переводим в строковый тип
		string_bits += bitset<8>(word[i]).to_string<char, char_traits<char>, allocator<char> >();
	}

	// строку разбиваем на символы и добавляем в массив
	vector<char> string_control_bits;
	vector<char> block_of_bits;

	for (int i = 0; i < string_bits.size(); i++) {
		block_of_bits.push_back(string_bits[i]);
		if (block_of_bits.size() % 8 == 0) {
			for (int j = 0; j < 4; j++) {
				block_of_bits.insert(block_of_bits.begin() + my_pow(2, j) - 1, '0');	//вставляем контрольные биты
			}
			calculate(block_of_bits);				//вычисляем контрольные биты

			string_control_bits.insert(				//добавляем в итоговый массив
				string_control_bits.end(),
				block_of_bits.begin(),
				block_of_bits.end()
			);
			block_of_bits = {};
		}
	}

	//из массива обратно в строковый тип
	string coded_string;
	for (int i = 0; i < string_control_bits.size(); i++) {
		coded_string += string_control_bits[i];
	}
	//итоговый закодированный массив с контрольными битами
	ofstream fout("signal.txt", ios_base::out);		//открываем файл для записи данных
	fout << coded_string;							//запись зашифрованных данных
	fout.close();

	cout << "Operation completed successfully";
	return 0;
}
